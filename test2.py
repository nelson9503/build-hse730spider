import json
import content_parser
import web_tools

url = "https://www.house730.com/buy-property-1997218.html"
lines = web_tools.get_web_text(url)
d = content_parser.content_parser(lines)

with open("test.json", 'w') as f:
    f.write(json.dumps(d))