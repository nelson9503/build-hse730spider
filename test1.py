import json
import cover_parser
import web_tools

url = "https://www.house730.com/buy/t1s1/"
lines = web_tools.get_web_text(url)
d = cover_parser.cover_page_parser(lines)

with open("test.json", 'w') as f:
    f.write(json.dumps(d))