
def cover_page_parser(lines: list) -> dict:
    
    results = {}

    for i in range(len(lines)):

        line = lines[i]

        if len(line) > 0 and line[0] == "[" and not "![]" in line:
            if "(/buy-property-" in line or "(/rent-property-" in line:
                result = {}
                
                # ad titles
                x = line.split("]")
                y = ""
                for i2 in range(len(x)-1):
                    y += x[i2]
                result["title"] = y[1:]

                # url
                x = line.split("](")[1]
                result["url"] = x[:-1]

                # id
                id = result["url"].replace("/rent-property-", "").replace("/buy-property-", "").replace(".html", "")

                line = lines[i+2]
                x = line.split("__")
                # area
                result["area"] = x[1].split("](")[0].replace("[", "").replace("]", "").replace(" ", "")
                # estate
                result["estate"] = x[2].split("](")[0].replace("[", "").replace("]", "").replace(" ", "")

                line = lines[i+4]
                x = line.split("  ")
                # netArea
                for y in x:
                    if "呎" in y:
                        result["netArea"] = y.replace("呎", "").replace(" ", "")
                # pricePerNetArea
                for y in x:
                    if "$" in y:
                        result["pricePerNetArea"] = y.replace("@", "").replace(" ", "")

                line = lines[i+6]
                x = line.split(" ")
                # buildArea
                for y in x:
                    if "呎" in y:
                        result["buildArea"] = y.replace("呎", "").replace(" ", "")
                # pricePerBuildArea
                for y in x:
                    if "$" in y:
                        result["pricePerBuildArea"] = y.replace("@", "").replace(" ", "")

                if not "buildArea" in result:
                    line = lines[i+6]
                else:
                    line = lines[i+8]
                x = line.split("|")
                # type
                result["type"] = x[0].replace(" ", "")
                # rooms
                try:
                    result["room"] = x[1].replace(" ", "")
                except:
                    pass
                # zone
                try:
                    result["zone"] = x[2].replace(" ", "")
                except:
                    pass

                # record
                results[id] = result
    
    return results