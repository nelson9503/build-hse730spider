
def selector(line: str) -> list:

    result = []

    for i in range(len(line)):

        if line[i] == "[":
            x = ""
            i2 = i + 1
            while not line[i2] == "]":
                x += line[i2]
                i2 += 1
            result.append(x)

    return result


def content_parser(lines: list) -> dict:

    result = {}

    for i in range(len(lines)):

        line = lines[i]

        if len(line) > 4 and line[:4] == "[主頁]":

            info = selector(line)
            result["adtype"] = info[1]
            result["region"] = info[2]
            result["district"] = info[3]

            line = lines[i+2]

        elif len(line) >= 8 and line[:8] == "  * ![](" and "img.house730.com" in line:
            if not "img" in result:
                result["img"] = {}
            url = line.split("  * ![](")[1].split(")")[0]
            result["img"][url] = 0

        elif len(line) >= 3 and line[:2] == "**" and "__" in line:
            if line[:3] == "**售" or line[:3] == "**租":
                result["price"] = line.split("__")[1].replace("*", "")

        elif len(line) >= 2 and line[:2] == "間隔":
            x = line.split("**")
            for y in x:
                if "年" in y:
                    result["buildingAge"] = y.replace(" ", "")
                    break

        elif len(line) >= 4 and line[:4] == "樓盤地址":
            result["address"] = line.replace("樓盤地址", "").replace(" ", "")

        elif len(line) >= 2 and line[:2] == "期數":
            result["phase"] = line.replace("期數", "").replace(" ", "")

        elif len(line) >= 5 and line[:5] == "座位及單位":
            result["unit"] = line.replace("座位及單位", "").replace(" ", "")

        elif len(line) >= 3 and line[:3] == "管理費":
            result["managementFee"] = line.replace("管理費", "").replace(" ", "")

        elif len(line) >= 11 and line[:11] == "###### 單位特色":
            i2 = i + 1
            x = ""
            while not "物業編號" in lines[i2]:
                if not lines[i2] == "":
                    x += lines[i2] + "\n"
                i2 += 1
            result["description"] = x

        elif len(line) >= 6 and line[:6] == "刊登/續期日":
            result["date"] = line.replace("刊登/續期日", "")

        elif len(line) >= 5 and line[:5] == "業主自讓盤" and "[ ![]() ](javascript" in lines[i+2]:
            i2 = i + 3
            while not "溫馨提示" in lines[i2]:
                if not lines[i2] == "" and not lines[i2][0] == " " and not "contactPerson" in result:
                    result["contactPerson"] = lines[i2]
                if len(lines[i2]) >= 2 and lines[i2][:2] == "__" and not "contactPhone" in result:
                    result["contactPhone"] = lines[i2].replace(
                        "__", "").replace(" ", "")
                i2 += 1

    return result
