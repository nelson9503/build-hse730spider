import requests
import html2text

def get_web_text(url:str) -> list:
    r = requests.get(url)
    lines = html2text.html2text(r.text).split("\n")
    return lines